/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.vehicles;

import uk.ac.tees.scedt.u0012604.vehicles.interfaces.Drivable;
import uk.ac.tees.scedt.u0012604.vehicles.interfaces.Steerable;
import uk.ac.tees.scedt.u0012604.engines.Engine;

/**
 *
 * @author steven
 */
public abstract class Vehicle implements Drivable, Steerable {
    private String colour;
    private String registration;
    private Engine engine;
        
    public Vehicle(final String colour, final String registration, final Engine engine) {
        this.colour = colour;
        this.registration = registration;
        this.engine = engine;
    }
    
    public void setColour(String newColour) {
        colour = newColour;
    }
    
    public void setRegistration(String newRegistration) {
        registration = newRegistration;
    }
    
    public void setEngine(Engine newEngine) {
        if(engine.compareTo(newEngine) != 0) {
            engine = newEngine;
        }
    }
    
    protected Engine getEngine() {
        return engine;
    }
    
    public String getColour() {
        return colour;
    }
    
    public String getRegistration() {
        return registration;
    }
}
