/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.vehicles;

import uk.ac.tees.scedt.u0012604.engines.Engine;

/**
 *
 * @author steven
 */
public class Car extends Vehicle {
        
    public Car(String colour, String registration, Engine engine) {
        super(colour, registration, engine);
    }

    @Override
    public String drive() {
        return "DRIVE: I like driving in my car - powered by a " + getEngine() + " generating " + getEngine().generatePower(0.76F) + " power.";
    }
    
    @Override
    public String steer() {
        return "STEER: I go in the direction that the driver turns the steering wheel.";
    }
    
    @Override
    public String toString() {
        return "A " + getColour() + " CAR (reg: " + getRegistration() + ") with a " + getEngine();
    }

}
