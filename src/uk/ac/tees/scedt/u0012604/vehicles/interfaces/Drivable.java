/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.vehicles.interfaces;

/**
 *
 * @author steven
 */
public interface Drivable {
    String drive();
}
