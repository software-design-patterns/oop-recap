/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.vehicles;

import uk.ac.tees.scedt.u0012604.engines.Engine;

/**
 *
 * @author steven
 */
public class Train extends Vehicle {
    
    public Train(final String colour, final String registration, final Engine engine) {
        super(colour, registration, engine);
    }

    @Override
    public String drive() {
        return "DRIVE: The train driver is driving the train -- the engine is a " + getEngine() + " generating " + getEngine().generatePower(1.F) + " of power.";
    }
    
    @Override
    public String steer() {
        return "STEER: I go in the direction of the rails - bit dull, but it works.";
    }
    
    @Override
    public String toString() {
        return "A " + getColour() + " TRAIN (reg: " + getRegistration() + ") with a " + getEngine();
    }
    
}
