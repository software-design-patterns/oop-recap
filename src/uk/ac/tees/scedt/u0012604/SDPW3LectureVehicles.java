/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604;

import uk.ac.tees.scedt.u0012604.vehicles.interfaces.Drivable;
import uk.ac.tees.scedt.u0012604.vehicles.interfaces.Steerable;
import uk.ac.tees.scedt.u0012604.vehicles.Vehicle;
import uk.ac.tees.scedt.u0012604.vehicles.Car;
import uk.ac.tees.scedt.u0012604.vehicles.Train;
import uk.ac.tees.scedt.u0012604.engines.HybridEngine;
import uk.ac.tees.scedt.u0012604.engines.ElectricEngine;
import uk.ac.tees.scedt.u0012604.engines.DieselEngine;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author steven
 */
public class SDPW3LectureVehicles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List<Vehicle> vehicles = buildVehicles();
        
        outputAsVehicles(vehicles);
        
        outputAsSteerables(vehicles);
        
        outputAsDrivables(vehicles);
    }
    
    private static List<Vehicle> buildVehicles() {
        banner("Building Vehicles");
        ArrayList<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(new Train("Purple and Yellow", "STHEN1823888", new DieselEngine("DE-TR-87215", 2280.0F)));
        vehicles.add(new Car("red", "AB18 AEP", new DieselEngine("DE1234", 120.0F)));
        vehicles.add(new Car("red", "LE16 PIE", new HybridEngine("HE0192", 180.0F)));
        vehicles.add(new Train("blue and green", "GNER827165836", new ElectricEngine("EE8172", 2000.0F, ElectricEngine.BatteryType.NickelHydride)));
        return vehicles;
    }
    
    // outputAsVehicles
    //
    // The list will contain Vehicle types.
    //
    private static void outputAsVehicles(final List<Vehicle> vehicles) {
        banner("Using Vehicle's full interface");
        for(Vehicle vehicle : vehicles) {
            // Know everything about vehicles
            //
            System.out.println(vehicle);
            System.out.println(vehicle.drive());
            System.out.println(vehicle.steer());
        } 
    }
    
    // outputAsSteerables
    //
    // Whatever the list contains, the objects must be Steerable types.
    //
    // Only the Steerable interface will be known.
    //
    private static void outputAsSteerables(final List<? extends Steerable> steerables) {
        banner("Only using Steerable interface");
        for(Steerable steerable : steerables) {
            // Only know about the steerable interface
            //
            System.out.println(steerable.steer());
        }        
    }
    
    // outputAsDrivables
    //
    // Whatever the list contains, the objects must be Drivable types.
    //
    // Only the Drivable interface will be known
    //
    private static void outputAsDrivables(final List<? extends Drivable> drivables) {
        banner("Only using Drivable interface");
        for(Drivable drivable : drivables) {
            // Only know about the drivable interface
            //
            System.out.println(drivable.drive());
        }        
    }
    
    private static String buildLine(final int length) {
        final StringBuilder sb = new StringBuilder(length);
        for(int n = 0; n < length; n++) {
            sb.append('-');
        }
        return sb.toString();
    }
    
    private static void banner(final String message) {
        final String line = buildLine(message.length() + 4);
        System.out.println(line + "\n  " + message + "\n" + line);
    }
    
}
