/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.engines;

/**
 *
 * @author steven
 */
public class PetrolEngine extends Engine {

    public PetrolEngine(String code, float maxHorsePower) {
        super(code, maxHorsePower);
    }
    
    @Override
    public float generatePower(float acceleratorPressure) {
        System.out.println("Using petrol to generate the power...");
        
        return acceleratorPressure * (maxHorsePower * 0.9F); // Warning: Completely made up petrol car power output formula!!!
    }
    
    @Override
    public String toString() {
        return "Petrol Engine";
    }
    
}
