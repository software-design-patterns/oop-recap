/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.engines;

/**
 *
 * @author steven
 */
public class DieselEngine extends Engine {

    public DieselEngine(String code, float maxHorsePower) {
        super(code, maxHorsePower);
    }
    
    @Override
    public float generatePower(float acceleratorPressure) {
        System.out.println("Using diesel to generate the power...");
        
        return maxHorsePower * acceleratorPressure; // Warning: Completely made up diesel car power output formula!!!
    }
    
    @Override
    public String toString() {
        return "Diesel Engine";
    }
}