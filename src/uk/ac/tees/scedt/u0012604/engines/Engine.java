/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.engines;

/**
 *
 * @author steven
 */
public abstract class Engine implements Comparable<Engine> {
    protected String code;
    protected float maxHorsePower;
    
    public Engine(String code, float maxHorsePower) {
        this.code = code;
        this.maxHorsePower = maxHorsePower;
    }
    
    public float getMaxHorsePower() {
        return maxHorsePower;
    }
    
    public String getCode() {
        return code;
    }
    
    /*
        A silly compareTo method
    */
    @Override
    public int compareTo(Engine otherEngine) {
        return code.compareTo(otherEngine.getCode());
    }
    
    public abstract float generatePower(float acceleratorPressure);
}
