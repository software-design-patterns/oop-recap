/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.engines;

/**
 *
 * @author steven
 */
public class ElectricEngine extends Engine {

    public enum BatteryType {
        LithionIon, MoltonSalt, NickelHydride, LithiumSulphur
    }
    
    private final BatteryType batteryType;
    
    public ElectricEngine(String code, float maxHorsePower) {
        this(code, maxHorsePower, BatteryType.LithionIon);
    }
    
    public ElectricEngine(String code, float maxHorsePower, BatteryType batteryType) {
        super(code, maxHorsePower);
        this.batteryType = batteryType;
    }
    
    @Override
    public float generatePower(float acceleratorPressure) {
        float power = 0.0F;
        switch(batteryType) {
            case LithionIon:
                System.out.println("A LithionIon battery is being used to generate the power...");
                power = 0.100F;
            case MoltonSalt:
                System.out.println("A Molton Salt battery is being used to generate the power...");
                power = 0.907F;
            case NickelHydride:
                System.out.println("A Nickel Hydride battery is being used to generate the power...");
                power =  0.803F;
            case LithiumSulphur:
                System.out.println("A Lithium Sulphure battery is being used to generate the power...");
                power = 0.866F;
        }
        return acceleratorPressure * maxHorsePower * power; // Warning: Completely made up electric car power output formula!!!
    }
    
    @Override
    public String toString() {
        return "Electric Engine";
    }
}
