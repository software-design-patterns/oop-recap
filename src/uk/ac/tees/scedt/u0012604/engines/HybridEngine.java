/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.scedt.u0012604.engines;

/**
 *
 * @author steven
 */
public class HybridEngine extends Engine {

    public HybridEngine(String code, float maxHorsePower) {
        super(code, maxHorsePower);
    }
    
    @Override
    public float generatePower(float acceleratorPressure) {
        float power = 0.0F;
        if(acceleratorPressure < 20.0F) {
            System.out.println("Using electric engine to generate the power...");
            power = 20.0F;
        }
        else {
            System.out.println("Using the petrol engine to generate the power now...");
            power = acceleratorPressure;
        }
            
        
        return power * maxHorsePower; // Warning: Completely made up hybrid car power output formula!!!
    }
    
    @Override
    public String toString() {
        return "Hybrid Engine";
    }
}